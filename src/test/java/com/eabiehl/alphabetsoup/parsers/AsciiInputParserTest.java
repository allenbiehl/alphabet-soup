package com.eabiehl.alphabetsoup.parsers;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Path;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.eabiehl.alphabetsoup.data.InputData;

/**
 * Unit test for {@see AsciiInputParser}.
 */
public class AsciiInputParserTest 
{    
	private static IInputParser parser;
	
	@BeforeClass
    public static void setUp(  ) {
		parser = new AsciiInputParser();
    }

	@AfterClass
    public static void tearDown(  ) {
		parser = null;
    }
	
    @Test
	public void parseValidResourceInput() {
    	Path path = FileSystems.getDefault().getPath("input", "complex.txt");
    	InputData inputData = parser.parse(path.toString());
    	
    	this.evalInputData(inputData);
	}
    
    @Test
	public void parseValidStreamInput() throws IOException {
    	ClassLoader classLoader = this.getClass().getClassLoader();
    	Path path = FileSystems.getDefault().getPath("input", "complex.txt");
    	InputStream inputStream = classLoader.getResourceAsStream(path.toString());
    	InputData inputData = parser.parse(inputStream);
    	
    	this.evalInputData(inputData);
	}
    
    @Test
	public void parseInvalidStreamInput() throws IOException {
    	InputStream inputStream = null;
    	InputData inputData = parser.parse(inputStream);
    	
    	assertTrue(inputData == null);
	}
    
	private void evalInputData(InputData inputData) {
    	assertTrue(inputData.getGrid().getTotalRows() == 5);
    	assertTrue(inputData.getGrid().getTotalColumns() == 5);
    	
    	assertTrue(inputData.getGrid().getCellValue(0, 0) == 'H');
    	assertTrue(inputData.getGrid().getCellValue(0, 1) == 'A');
    	assertTrue(inputData.getGrid().getCellValue(0, 2) == 'S');
    	assertTrue(inputData.getGrid().getCellValue(0, 3) == 'D');
    	assertTrue(inputData.getGrid().getCellValue(0, 4) == 'F');
    	assertTrue(inputData.getGrid().getCellValue(1, 0) == 'G');
    	assertTrue(inputData.getGrid().getCellValue(1, 1) == 'E');
    	assertTrue(inputData.getGrid().getCellValue(1, 2) == 'Y');
    	assertTrue(inputData.getGrid().getCellValue(1, 3) == 'B');
    	assertTrue(inputData.getGrid().getCellValue(1, 4) == 'H');
    	assertTrue(inputData.getGrid().getCellValue(2, 0) == 'J');
    	assertTrue(inputData.getGrid().getCellValue(2, 1) == 'K');
    	assertTrue(inputData.getGrid().getCellValue(2, 2) == 'L');
    	assertTrue(inputData.getGrid().getCellValue(2, 3) == 'Z');
    	assertTrue(inputData.getGrid().getCellValue(2, 4) == 'X');
    	assertTrue(inputData.getGrid().getCellValue(3, 0) == 'C');
    	assertTrue(inputData.getGrid().getCellValue(3, 1) == 'V');
    	assertTrue(inputData.getGrid().getCellValue(3, 2) == 'B');
    	assertTrue(inputData.getGrid().getCellValue(3, 3) == 'L');
    	assertTrue(inputData.getGrid().getCellValue(3, 4) == 'N');
    	assertTrue(inputData.getGrid().getCellValue(4, 0) == 'G');
    	assertTrue(inputData.getGrid().getCellValue(4, 1) == 'O');
    	assertTrue(inputData.getGrid().getCellValue(4, 2) == 'O');
    	assertTrue(inputData.getGrid().getCellValue(4, 3) == 'D');
    	assertTrue(inputData.getGrid().getCellValue(4, 4) == 'O');
    	
    	assertTrue(inputData.getWords().size() == 3);
    	
    	assertTrue(inputData.getWords().get(0).getValue().equals("HELLO"));
    	assertTrue(inputData.getWords().get(1).getValue().equals("GOOD"));
    	assertTrue(inputData.getWords().get(2).getValue().equals("BYE"));
	}
}
