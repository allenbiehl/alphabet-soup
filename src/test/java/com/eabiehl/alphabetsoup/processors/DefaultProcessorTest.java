package com.eabiehl.alphabetsoup.processors;

import static org.junit.Assert.assertTrue;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.eabiehl.alphabetsoup.data.WordMatch;

/**
 * Unit test for {@see DefaultProcessor}.
 */
public class DefaultProcessorTest 
{    
	private static IProcessor processor;
	
	@BeforeClass
    public static void setUp(  ) {
    	processor = new DefaultProcessor();
    }

	@AfterClass
    public static void tearDown(  ) {
    	processor = null;
    }
	
    @Test
	public void processInvalidInput() {

    	List<WordMatch> wordMatches = processor.process("invalid");
    	
    	assertTrue(wordMatches.size() == 0);
	}
	
    @Test
	public void processHelloInput() {
    	Path path = FileSystems.getDefault().getPath("input", "complex.txt");
    	List<WordMatch> wordMatches = processor.process(path.toString());
    	
    	assertTrue(wordMatches.size() == 3);
    	
    	assertTrue(wordMatches.get(0).getWord().getValue().equals("HELLO"));
    	
    	assertTrue(wordMatches.get(0).getCells().get(0).getRowIdx() == 0);
    	assertTrue(wordMatches.get(0).getCells().get(0).getColumnIdx() == 0);
    	assertTrue(wordMatches.get(0).getCells().get(0).getValue() == 'H');
    	
    	assertTrue(wordMatches.get(0).getCells().get(1).getRowIdx() == 1);
    	assertTrue(wordMatches.get(0).getCells().get(1).getColumnIdx() == 1);
    	assertTrue(wordMatches.get(0).getCells().get(1).getValue() == 'E');
    	
    	assertTrue(wordMatches.get(0).getCells().get(2).getRowIdx() == 2);
    	assertTrue(wordMatches.get(0).getCells().get(2).getColumnIdx() == 2);
    	assertTrue(wordMatches.get(0).getCells().get(2).getValue() == 'L');
    	
    	assertTrue(wordMatches.get(0).getCells().get(3).getRowIdx() == 3);
    	assertTrue(wordMatches.get(0).getCells().get(3).getColumnIdx() == 3);
    	assertTrue(wordMatches.get(0).getCells().get(3).getValue() == 'L');
    	
    	assertTrue(wordMatches.get(0).getCells().get(4).getRowIdx() == 4);
    	assertTrue(wordMatches.get(0).getCells().get(4).getColumnIdx() == 4);
    	assertTrue(wordMatches.get(0).getCells().get(4).getValue() == 'O');
	}
    
    @Test
	public void processGoodInput() {
    	Path path = FileSystems.getDefault().getPath("input", "complex.txt");
    	List<WordMatch> wordMatches = processor.process(path.toString());
    	
    	assertTrue(wordMatches.get(1).getWord().getValue().equals("GOOD"));
    	
    	assertTrue(wordMatches.get(1).getCells().get(0).getRowIdx() == 4);
    	assertTrue(wordMatches.get(1).getCells().get(0).getColumnIdx() == 0);
    	assertTrue(wordMatches.get(1).getCells().get(0).getValue() == 'G');
    	
    	assertTrue(wordMatches.get(1).getCells().get(1).getRowIdx() == 4);
    	assertTrue(wordMatches.get(1).getCells().get(1).getColumnIdx() == 1);
    	assertTrue(wordMatches.get(1).getCells().get(1).getValue() == 'O');
    	
    	assertTrue(wordMatches.get(1).getCells().get(2).getRowIdx() == 4);
    	assertTrue(wordMatches.get(1).getCells().get(2).getColumnIdx() == 2);
    	assertTrue(wordMatches.get(1).getCells().get(2).getValue() == 'O');
    	
    	assertTrue(wordMatches.get(1).getCells().get(3).getRowIdx() == 4);
    	assertTrue(wordMatches.get(1).getCells().get(3).getColumnIdx() == 3);
    	assertTrue(wordMatches.get(1).getCells().get(3).getValue() == 'D');
	}
    
    @Test
	public void processByeInput() {
    	Path path = FileSystems.getDefault().getPath("input", "complex.txt");
    	List<WordMatch> wordMatches = processor.process(path.toString());
    	
    	assertTrue(wordMatches.get(2).getWord().getValue().equals("BYE"));
    	
    	assertTrue(wordMatches.get(2).getCells().get(0).getRowIdx() == 1);
    	assertTrue(wordMatches.get(2).getCells().get(0).getColumnIdx() == 3);
    	assertTrue(wordMatches.get(2).getCells().get(0).getValue() == 'B');
    	
    	assertTrue(wordMatches.get(2).getCells().get(1).getRowIdx() == 1);
    	assertTrue(wordMatches.get(2).getCells().get(1).getColumnIdx() == 2);
    	assertTrue(wordMatches.get(2).getCells().get(1).getValue() == 'Y');
    	
    	assertTrue(wordMatches.get(2).getCells().get(2).getRowIdx() == 1);
    	assertTrue(wordMatches.get(2).getCells().get(2).getColumnIdx() == 1);
    	assertTrue(wordMatches.get(2).getCells().get(2).getValue() == 'E');
	}
    
    @Test
	public void processSpacesInput() {
    	Path path = FileSystems.getDefault().getPath("input", "spaces.txt");
    	List<WordMatch> wordMatches = processor.process(path.toString());
    	
    	assertTrue(wordMatches.size() == 1);
    	
    	assertTrue(wordMatches.get(0).getWord().getValue().equals("ONE AND DONE"));
    	
    	assertTrue(wordMatches.get(0).getCells().get(0).getRowIdx() == 0);
    	assertTrue(wordMatches.get(0).getCells().get(0).getColumnIdx() == 9);
    	assertTrue(wordMatches.get(0).getCells().get(0).getValue() == 'O');
    	
    	assertTrue(wordMatches.get(0).getCells().get(1).getRowIdx() == 1);
    	assertTrue(wordMatches.get(0).getCells().get(1).getColumnIdx() == 8);
    	assertTrue(wordMatches.get(0).getCells().get(1).getValue() == 'N');
    	
    	assertTrue(wordMatches.get(0).getCells().get(2).getRowIdx() == 2);
    	assertTrue(wordMatches.get(0).getCells().get(2).getColumnIdx() == 7);
    	assertTrue(wordMatches.get(0).getCells().get(2).getValue() == 'E');
    	
    	assertTrue(wordMatches.get(0).getCells().get(3).getRowIdx() == 3);
    	assertTrue(wordMatches.get(0).getCells().get(3).getColumnIdx() == 6);
    	assertTrue(wordMatches.get(0).getCells().get(3).getValue() == 'A');
    	
    	assertTrue(wordMatches.get(0).getCells().get(4).getRowIdx() == 4);
    	assertTrue(wordMatches.get(0).getCells().get(4).getColumnIdx() == 5);
    	assertTrue(wordMatches.get(0).getCells().get(4).getValue() == 'N');
    	
    	assertTrue(wordMatches.get(0).getCells().get(5).getRowIdx() == 5);
    	assertTrue(wordMatches.get(0).getCells().get(5).getColumnIdx() == 4);
    	assertTrue(wordMatches.get(0).getCells().get(5).getValue() == 'D');
    	
    	assertTrue(wordMatches.get(0).getCells().get(6).getRowIdx() == 6);
    	assertTrue(wordMatches.get(0).getCells().get(6).getColumnIdx() == 3);
    	assertTrue(wordMatches.get(0).getCells().get(6).getValue() == 'D');
    	
    	assertTrue(wordMatches.get(0).getCells().get(7).getRowIdx() == 7);
    	assertTrue(wordMatches.get(0).getCells().get(7).getColumnIdx() == 2);
    	assertTrue(wordMatches.get(0).getCells().get(7).getValue() == 'O');
    	
    	assertTrue(wordMatches.get(0).getCells().get(8).getRowIdx() == 8);
    	assertTrue(wordMatches.get(0).getCells().get(8).getColumnIdx() == 1);
    	assertTrue(wordMatches.get(0).getCells().get(8).getValue() == 'N');
    	
    	assertTrue(wordMatches.get(0).getCells().get(9).getRowIdx() == 9);
    	assertTrue(wordMatches.get(0).getCells().get(9).getColumnIdx() == 0);
    	assertTrue(wordMatches.get(0).getCells().get(9).getValue() == 'E');
	}
}
