package com.eabiehl.alphabetsoup.matchers;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.eabiehl.alphabetsoup.data.Grid;
import com.eabiehl.alphabetsoup.data.Word;
import com.eabiehl.alphabetsoup.data.WordMatch;

/**
 * Unit test for {@see RadialWordMatcher}.
 */
public class RadialWordMatcherTest 
{    
	private static IWordMatcher matcher;
	
	@BeforeClass
    public static void setUp(  ) {
		matcher = new RadialWordMatcher();
    }

	@AfterClass
    public static void tearDown(  ) {
		matcher = null;
    }
	
    @Test
	public void checkTopBottomDirection() {
    	
    	Grid grid = new Grid(2, 2);
    	grid.setCellValue(0, 0, 'O');
    	grid.setCellValue(0, 1, ' ');
    	grid.setCellValue(1, 0, 'N');
    	grid.setCellValue(1, 1, ' ');
    	
    	List<Word> words = new ArrayList<Word>();
    	words.add(new Word("ON"));
    	
    	List<WordMatch> matches = matcher.findMatches(grid, words);
    	
    	assertTrue(matches.size() == 1);
    	
    	assertTrue(matches.get(0).getWord().getValue().equals("ON"));
    	
    	assertTrue(matches.get(0).getCells().get(0).getRowIdx() == 0);
    	assertTrue(matches.get(0).getCells().get(0).getColumnIdx() == 0);
    	assertTrue(matches.get(0).getCells().get(0).getValue() == 'O');
    	
    	assertTrue(matches.get(0).getCells().get(1).getRowIdx() == 1);
    	assertTrue(matches.get(0).getCells().get(1).getColumnIdx() == 0);
    	assertTrue(matches.get(0).getCells().get(1).getValue() == 'N');
	}
    
    @Test
	public void checkBottomTopDirection() {
    	
    	Grid grid = new Grid(2, 2);
    	grid.setCellValue(0, 0, 'N');
    	grid.setCellValue(0, 1, ' ');
    	grid.setCellValue(1, 0, 'O');
    	grid.setCellValue(1, 1, ' ');
    	
    	List<Word> words = new ArrayList<Word>();
    	words.add(new Word("ON"));
    	
    	List<WordMatch> matches = matcher.findMatches(grid, words);
    	
    	assertTrue(matches.size() == 1);
    	
    	assertTrue(matches.get(0).getWord().getValue().equals("ON"));
    	
    	assertTrue(matches.get(0).getCells().get(0).getRowIdx() == 1);
    	assertTrue(matches.get(0).getCells().get(0).getColumnIdx() == 0);
    	assertTrue(matches.get(0).getCells().get(0).getValue() == 'O');
    	
    	assertTrue(matches.get(0).getCells().get(1).getRowIdx() == 0);
    	assertTrue(matches.get(0).getCells().get(1).getColumnIdx() == 0);
    	assertTrue(matches.get(0).getCells().get(1).getValue() == 'N');
	}
    
    @Test
	public void checkLeftRightDirection() {
    	
    	Grid grid = new Grid(2, 2);
    	grid.setCellValue(0, 0, 'O');
    	grid.setCellValue(0, 1, 'N');
    	grid.setCellValue(1, 0, ' ');
    	grid.setCellValue(1, 1, ' ');
    	
    	List<Word> words = new ArrayList<Word>();
    	words.add(new Word("ON"));
    	
    	List<WordMatch> matches = matcher.findMatches(grid, words);
    	
    	assertTrue(matches.size() == 1);
    	
    	assertTrue(matches.get(0).getWord().getValue().equals("ON"));
    	
    	assertTrue(matches.get(0).getCells().get(0).getRowIdx() == 0);
    	assertTrue(matches.get(0).getCells().get(0).getColumnIdx() == 0);
    	assertTrue(matches.get(0).getCells().get(0).getValue() == 'O');
    	
    	assertTrue(matches.get(0).getCells().get(1).getRowIdx() == 0);
    	assertTrue(matches.get(0).getCells().get(1).getColumnIdx() == 1);
    	assertTrue(matches.get(0).getCells().get(1).getValue() == 'N');
	}
    
    @Test
	public void checkRightLeftDirection() {
    	
    	Grid grid = new Grid(2, 2);
    	grid.setCellValue(0, 0, ' ');
    	grid.setCellValue(0, 1, ' ');
    	grid.setCellValue(1, 0, 'N');
    	grid.setCellValue(1, 1, 'O');
    	
    	List<Word> words = new ArrayList<Word>();
    	words.add(new Word("ON"));
    	
    	List<WordMatch> matches = matcher.findMatches(grid, words);
    	
    	assertTrue(matches.size() == 1);
    	
    	assertTrue(matches.get(0).getWord().getValue().equals("ON"));
    	
    	assertTrue(matches.get(0).getCells().get(0).getRowIdx() == 1);
    	assertTrue(matches.get(0).getCells().get(0).getColumnIdx() == 1);
    	assertTrue(matches.get(0).getCells().get(0).getValue() == 'O');
    	
    	assertTrue(matches.get(0).getCells().get(1).getRowIdx() == 1);
    	assertTrue(matches.get(0).getCells().get(1).getColumnIdx() == 0);
    	assertTrue(matches.get(0).getCells().get(1).getValue() == 'N');
	}
    
    @Test
	public void checkTopLeftBottomRightDirection() {
    	
    	Grid grid = new Grid(2, 2);
    	grid.setCellValue(0, 0, 'O');
    	grid.setCellValue(0, 1, ' ');
    	grid.setCellValue(1, 0, ' ');
    	grid.setCellValue(1, 1, 'N');
    	
    	List<Word> words = new ArrayList<Word>();
    	words.add(new Word("ON"));
    	
    	List<WordMatch> matches = matcher.findMatches(grid, words);
    	
    	assertTrue(matches.size() == 1);
    	
    	assertTrue(matches.get(0).getWord().getValue().equals("ON"));
    	
    	assertTrue(matches.get(0).getCells().get(0).getRowIdx() == 0);
    	assertTrue(matches.get(0).getCells().get(0).getColumnIdx() == 0);
    	assertTrue(matches.get(0).getCells().get(0).getValue() == 'O');
    	
    	assertTrue(matches.get(0).getCells().get(1).getRowIdx() == 1);
    	assertTrue(matches.get(0).getCells().get(1).getColumnIdx() == 1);
    	assertTrue(matches.get(0).getCells().get(1).getValue() == 'N');
	}
    
    @Test
	public void checkBottomRightTopLeftDirection() {
    	
    	Grid grid = new Grid(2, 2);
    	grid.setCellValue(0, 0, 'N');
    	grid.setCellValue(0, 1, ' ');
    	grid.setCellValue(1, 0, ' ');
    	grid.setCellValue(1, 1, 'O');
    	
    	List<Word> words = new ArrayList<Word>();
    	words.add(new Word("ON"));
    	
    	List<WordMatch> matches = matcher.findMatches(grid, words);
    	
    	assertTrue(matches.size() == 1);
    	
    	assertTrue(matches.get(0).getWord().getValue().equals("ON"));
    	
    	assertTrue(matches.get(0).getCells().get(0).getRowIdx() == 1);
    	assertTrue(matches.get(0).getCells().get(0).getColumnIdx() == 1);
    	assertTrue(matches.get(0).getCells().get(0).getValue() == 'O');
    	
    	assertTrue(matches.get(0).getCells().get(1).getRowIdx() == 0);
    	assertTrue(matches.get(0).getCells().get(1).getColumnIdx() == 0);
    	assertTrue(matches.get(0).getCells().get(1).getValue() == 'N');
	}
    
    @Test
	public void checkBottomLeftTopRightDirection() {
    	
    	Grid grid = new Grid(2, 2);
    	grid.setCellValue(0, 0, ' ');
    	grid.setCellValue(0, 1, 'N');
    	grid.setCellValue(1, 0, 'O');
    	grid.setCellValue(1, 1, ' ');
    	
    	List<Word> words = new ArrayList<Word>();
    	words.add(new Word("ON"));
    	
    	List<WordMatch> matches = matcher.findMatches(grid, words);
    	
    	assertTrue(matches.size() == 1);
    	
    	assertTrue(matches.get(0).getWord().getValue().equals("ON"));
    	
    	assertTrue(matches.get(0).getCells().get(0).getRowIdx() == 1);
    	assertTrue(matches.get(0).getCells().get(0).getColumnIdx() == 0);
    	assertTrue(matches.get(0).getCells().get(0).getValue() == 'O');
    	
    	assertTrue(matches.get(0).getCells().get(1).getRowIdx() == 0);
    	assertTrue(matches.get(0).getCells().get(1).getColumnIdx() == 1);
    	assertTrue(matches.get(0).getCells().get(1).getValue() == 'N');
	}
    
    @Test
	public void checkTopRightBottomLeftDirection() {
    	
    	Grid grid = new Grid(2, 2);
    	grid.setCellValue(0, 0, ' ');
    	grid.setCellValue(0, 1, 'O');
    	grid.setCellValue(1, 0, 'N');
    	grid.setCellValue(1, 1, ' ');
    	
    	List<Word> words = new ArrayList<Word>();
    	words.add(new Word("ON"));
    	
    	List<WordMatch> matches = matcher.findMatches(grid, words);
    	
    	assertTrue(matches.size() == 1);
    	
    	assertTrue(matches.get(0).getWord().getValue().equals("ON"));
    	
    	assertTrue(matches.get(0).getCells().get(0).getRowIdx() == 0);
    	assertTrue(matches.get(0).getCells().get(0).getColumnIdx() == 1);
    	assertTrue(matches.get(0).getCells().get(0).getValue() == 'O');
    	
    	assertTrue(matches.get(0).getCells().get(1).getRowIdx() == 1);
    	assertTrue(matches.get(0).getCells().get(1).getColumnIdx() == 0);
    	assertTrue(matches.get(0).getCells().get(1).getValue() == 'N');
	}
}
