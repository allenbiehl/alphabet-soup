package com.eabiehl.alphabetsoup;

import static org.junit.Assert.assertTrue;

import java.nio.file.FileSystems;
import java.nio.file.Path;

import org.junit.Test;

/**
 * Unit test for {@see Program}.
 */
public class ProgramTest 
{    
    @Test
	public void runUnspecified() {
    	
    	Program.main(new String[] {});
    	
        assertTrue(true);
	}
    
    @Test
	public void runSpaces() {
    	
    	Path path = FileSystems.getDefault().getPath("input", "spaces.txt");
    	Program.main(new String[] {path.toString()});
    	
        assertTrue(true);
	}
    
    @Test
	public void runSimple() {
    	
    	Path path = FileSystems.getDefault().getPath("input", "simple.txt");
    	Program.main(new String[] {path.toString()});
    	
        assertTrue(true);
	}
    
    @Test
	public void runComplex() {
    	
    	Path path = FileSystems.getDefault().getPath("input", "complex.txt");
    	Program.main(new String[] {path.toString()});
    	
        assertTrue(true);
	}
}
