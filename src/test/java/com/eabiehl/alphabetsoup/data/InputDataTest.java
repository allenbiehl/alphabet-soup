package com.eabiehl.alphabetsoup.data;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * Unit test for {@see InputData}.
 */
public class InputDataTest 
{    
    @Test
	public void createInputData() {
    	Grid grid = new Grid();
    	List<Word> words = new ArrayList<Word>();
    	InputData inputData = new InputData(grid, words);
    	
        assertTrue(inputData.getGrid() == grid);
        assertTrue(inputData.getWords() == words);
	}
}
