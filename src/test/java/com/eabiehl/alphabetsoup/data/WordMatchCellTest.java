package com.eabiehl.alphabetsoup.data;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for {@see WordMatchCell}.
 */
public class WordMatchCellTest 
{    
    @Test
	public void createWordMatchCell() {
    	int rowIdx = 0;
    	int colIdx = 0;
    	char value = 'A';
    	WordMatchCell wordMatchCell = new WordMatchCell(rowIdx, colIdx, value);
    	
        assertTrue(wordMatchCell.getRowIdx() == rowIdx);
        assertTrue(wordMatchCell.getColumnIdx() == colIdx);
        assertTrue(wordMatchCell.getValue() == value);
	}
}
