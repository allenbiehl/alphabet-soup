package com.eabiehl.alphabetsoup.data;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for {@see Word}.
 */
public class WordTest 
{    
    @Test
	public void createSingleWord() {
    	String value = "VALUE";
    	String compressedValue = "VALUE";
    	Word word = new Word(value);
    	
        assertTrue(word.getValue() == value);
        assertTrue(word.getCompressedValue().equals(compressedValue));
        assertTrue(word.toString() == value);
	}
    
    @Test
	public void createMultiWord() {
    	String value = "MY TEST VALUE";
    	String compressedValue = "MYTESTVALUE";
    	Word word = new Word(value);
    	
        assertTrue(word.getValue() == value);
        assertTrue(word.getCompressedValue().equals(compressedValue));
        assertTrue(word.toString() == value);
	}
}
