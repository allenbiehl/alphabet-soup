package com.eabiehl.alphabetsoup.data;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

/**
 * Unit test for {@see Grid}.
 */
public class GridTest 
{    
    @Test
	public void createEmptyGrid() {
    	Grid grid = new Grid();
    	
        assertTrue(grid.getTotalRows() == 0);
        assertTrue(grid.getTotalColumns() == 0);
        assertTrue(grid.getCells().length == 0);
	}
    
    @Test
	public void createFixedGrid() {
    	Grid grid = new Grid(10,10);
    	
        assertTrue(grid.getTotalRows() == 10);
        assertTrue(grid.getTotalColumns() == 10);
        assertTrue(grid.getCells().length == 10);
        assertTrue(grid.getCells()[0].length == 10);
	}
    
    @Test
	public void setInBoundsValues() {
    	Grid grid = new Grid(1,1);
    	
        assertTrue(grid.getCellValue(0, 0) == 0);
        assertTrue(grid.getCells()[0][0] == 0);
        
        assertTrue(grid.setCellValue(0, 0, 'A'));
        
        assertTrue(grid.getCellValue(0, 0) == 'A');
        assertTrue(grid.getCells()[0][0] == 'A');
	}
    
    @Test
	public void setOutBoundsValues() {
    	Grid grid = new Grid(1,1);
    	
        assertFalse(grid.setCellValue(0, 1, 'A'));
        assertTrue(grid.getCellValue(0, 1) == 0);

        assertFalse(grid.setCellValue(0, -1, 'A'));
        assertTrue(grid.getCellValue(0, -1) == 0);
        
        assertFalse(grid.setCellValue(1, 0, 'A'));
        assertTrue(grid.getCellValue(1, 0) == 0);
        
        assertFalse(grid.setCellValue(-1, 0, 'A'));
        assertTrue(grid.getCellValue(-1, 0) == 0);
	}
}
