package com.eabiehl.alphabetsoup.data;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * Unit test for {@see WordMatch}.
 */
public class WordMatchTest 
{    
    @Test
	public void createWordMatch() {
    	Word word = new Word("TEST");
    	List<WordMatchCell> cells = new ArrayList<WordMatchCell>();
    	WordMatch wordMatch = new WordMatch(word, cells);
    	
        assertTrue(wordMatch.getWord() == word);
        assertTrue(wordMatch.getCells() == cells);
	}
}
