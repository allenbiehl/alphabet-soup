package com.eabiehl.alphabetsoup.formatters;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.eabiehl.alphabetsoup.data.Word;
import com.eabiehl.alphabetsoup.data.WordMatch;
import com.eabiehl.alphabetsoup.data.WordMatchCell;

/**
 * Unit test for {@see DefaultWordMatchFormatter}.
 */
public class DefaultWordMatchFormatterTest 
{    
	private static IWordMatchFormatter formatter;
	
	@BeforeClass
    public static void setUp(  ) {
    	formatter = new DefaultWordMatchFormatter();
    }

	@AfterClass
    public static void tearDown(  ) {
    	formatter = null;
    }
	
    @Test
	public void formatWordMatch() {
    	Word word = new Word("TEST");
    	List<WordMatchCell> cells = new ArrayList<WordMatchCell>();
    	cells.add(new WordMatchCell(0, 0, 'T'));
    	cells.add(new WordMatchCell(0, 1, 'E'));
    	cells.add(new WordMatchCell(0, 2, 'S'));
    	cells.add(new WordMatchCell(0, 3, 'T'));
    	WordMatch wordMatch = new WordMatch(word, cells);
    	String result = formatter.format(wordMatch);
    	
        assertTrue(result.equals("TEST 0:0 0:3"));
	}
}
