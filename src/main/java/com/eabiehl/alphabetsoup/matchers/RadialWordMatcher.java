package com.eabiehl.alphabetsoup.matchers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eabiehl.alphabetsoup.data.Grid;
import com.eabiehl.alphabetsoup.data.Word;
import com.eabiehl.alphabetsoup.data.WordMatch;
import com.eabiehl.alphabetsoup.data.WordMatchCell;

/**
 * Word matcher implementation used to find words contained within a two dimensional grid. 
 * The {@see RadialWordMatcher} attempts to find word matches based on the starting origin 
 * of the word. The grid is scanned for the first letter of the word, and once its found,
 * the matcher attempts to match the word contained on any of the 8 grid axis.
 * 
 * <p>Axis Searched</p>
 * <ul>
 * <li>Vertical Top to Bottom</li>
 * <li>Vertical Bottom to Top</li>
 * <li>Horizontal Left to Right</li>
 * <li>Horizontal Right to Left</li>
 * <li>Diagonal Bottom Left to Top Right</li>
 * <li>Diagonal Top Right to Bottom Left</li>
 * <li>Diagonal Top Left to Bottom Right</li>
 * <li>Diagonal Bottom Right to Top Left</li>
 * </ul>
 * 
 * @author eabiehl
 */
public class RadialWordMatcher implements IWordMatcher {
	
	/**
	 * X axis.
	 */
	static int[] x = { -1, -1, -1, 0, 0, 1, 1, 1 };
	
	/**
	 * Y axis.
	 */
	static int[] y = { -1, 0, 1, -1, 1, -1, 0, 1 };
	
	/**
     * Logger instance.
	 */
    private Logger logger;
   
    /**
	 * Constructor for initializing a {@see RadialWordMatcher} instance.
     */
	public RadialWordMatcher() {
	    this.logger = LoggerFactory.getLogger(RadialWordMatcher.class);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<WordMatch> findMatches(Grid grid, List<Word> words) {
		this.logger.debug(String.format("Find matches for %s word(s)", words.size()));
		List<WordMatch> matches = new ArrayList<WordMatch>();
		List<WordMatchCell> wordMatchCells;
		
		for (Word word : words) {
			String value = word.getCompressedValue();
			
			// Find pattern forward
			wordMatchCells = this.findWordMatch(grid, value);
			
			if (wordMatchCells != null) {
				matches.add(new WordMatch(word, wordMatchCells));
				continue;
			}
		}
		return matches;
	}
	
	/**
	 * Internal method for finding the compressed word within a grid, which
	 * can be either in forward or reverse order. 
	 * 
	 * @param grid Two dimensional grid to search.
	 * @param word Word to search for contained in the grid.
	 * @return List of {@WordMatchCell} instances where each character of the word was found.
	 */
	private List<WordMatchCell> findWordMatch(Grid grid, String word) {
		char[][] cells = grid.getCells();
		char firstChar = word.charAt(0);
		List<WordMatchCell> wordMatchCells = null;
		
		// Iterate rows
		for (int rowIdx = 0; rowIdx < grid.getTotalRows(); rowIdx++) {
			
			// Iterate columns
			for (int colIdx = 0; colIdx < grid.getTotalColumns(); colIdx++) {
				
				// Check if cell character matches word's first character
				if (cells[rowIdx][colIdx] == firstChar) {
					wordMatchCells = this.findWordMatchByOrigin(grid, word, rowIdx, colIdx);
					
					// First word match found - Stop processing
					if (wordMatchCells != null) {
						break;
					}
				}
			}
			
			if (wordMatchCells != null) {
				break;
			}
		}
	    return wordMatchCells;
	}
	
	/**
	 * Internal method for finding the compressed word within a grid based
	 * on the location of the first character contained in the word. Once the
	 * origin is located, we rotate across all 8 axis to match the word and
	 * locate all word cell locations.
	 * 
	 * For example [H]ELLO.
	 * 
	 * @param grid Two dimensional grid to search.
	 * @param word Word to search for in the grid.
	 * @param originRowIdx Row index where the first character was found.
	 * @param originColIdx Column index where the first character was found.
	 * @return List of {@WordMatchCell} instances where each character of the word was found.
	 */
	private List<WordMatchCell> findWordMatchByOrigin(Grid grid, String word, int originRowIdx, int originColIdx) {
		char[][] cells = grid.getCells();
		int totalRows = grid.getTotalRows();
		int totalColumns = grid.getTotalColumns();
		int wordLength = word.length();
		List<WordMatchCell> wordMatchCells;

		// Iterate through all directions
		for (int dir = 0; dir < 8; dir++) {
			int rowIdx = originRowIdx + x[dir];
			int colIdx = originColIdx + y[dir];
			wordMatchCells = new ArrayList<WordMatchCell>();
			wordMatchCells.add(new WordMatchCell(originRowIdx, originColIdx, cells[originRowIdx][originColIdx]));
			
			for (int segLength = 1; segLength < wordLength; segLength++) {
				
				// Check if cell is outside grid boundary
				if (rowIdx >= totalRows || rowIdx < 0 || colIdx >= totalColumns || colIdx < 0) {
					break;
				}

				char cellValue = cells[rowIdx][colIdx];
				
				// Check if no cell match, rotate direction
				if (cellValue != word.charAt(segLength)) {
					break;
				}

				wordMatchCells.add(new WordMatchCell(rowIdx, colIdx, cellValue));
				
				// Increment direction
				rowIdx += x[dir];
				colIdx += y[dir];
			}
			
			if (wordMatchCells.size() == wordLength) {
				this.logger.debug(String.format("Word match found, word: %s", word));
				
				for (WordMatchCell cell : wordMatchCells) {
					this.logger.debug(String.format("Word match cell, row idx: %s, col idx: %s, value: %s", 
						cell.getRowIdx(), cell.getColumnIdx(), cell.getValue()));
				}
				
				return wordMatchCells;
			}
		}
		return null;
	}
}