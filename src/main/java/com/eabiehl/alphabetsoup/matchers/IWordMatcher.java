package com.eabiehl.alphabetsoup.matchers;

import java.util.List;

import com.eabiehl.alphabetsoup.data.Grid;
import com.eabiehl.alphabetsoup.data.Word;
import com.eabiehl.alphabetsoup.data.WordMatch;

/**
 * Interface used to find words contained within a two dimensional grid. 
 * 
 * <p>Rules</p>
 * <ul>
 * <li>Within the grid of characters, the words may appear vertical, horizontal or diagonal.</li>
 * <li>Within the grid of characters, the words may appear forwards or backwards.</li>
 * <li>Words that have spaces in them will not include spaces when hidden in the grid of characters.</li>
 * </ul>
 * 
 * @author eabiehl
 */
public interface IWordMatcher {

	/**
	 * Find the specified words contained within the two dimensional grid.
	 * 
	 * @param grid Two dimensional grid that contains the words.
	 * @param words Words to find within the grid.
	 * @return List of {@see WordMatch} instances that were found in the grid.
	 */
	List<WordMatch> findMatches(Grid grid, List<Word> words);
}
