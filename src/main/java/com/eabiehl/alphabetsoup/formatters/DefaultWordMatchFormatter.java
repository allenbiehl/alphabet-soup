package com.eabiehl.alphabetsoup.formatters;

import java.util.List;

import com.eabiehl.alphabetsoup.data.Word;
import com.eabiehl.alphabetsoup.data.WordMatch;
import com.eabiehl.alphabetsoup.data.WordMatchCell;

/**
 * Default implementation used to format the output of a {@see WordMatch} instance. 
 * 
 * @author eabiehl
 */
public class DefaultWordMatchFormatter implements IWordMatchFormatter {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String format(WordMatch wordMatch) {
		Word word = wordMatch.getWord();
		List<WordMatchCell> cells = wordMatch.getCells();
		String wordValue = null;
	    String cellStartValue = null;
	    String cellEndValue = null;

	    if (word != null && cells != null) {
	    	wordValue = word.getValue();

	    	WordMatchCell cellStart = cells.get(0);
	    	cellStartValue = String.format("%s:%s", cellStart.getRowIdx(), cellStart.getColumnIdx());
	    	
	    	WordMatchCell cellEnd = cells.get(cells.size() - 1);
	    	cellEndValue = String.format("%s:%s", cellEnd.getRowIdx(), cellEnd.getColumnIdx());
	    }
	    
	    return String.format("%s %s %s", wordValue, cellStartValue, cellEndValue);
	}
}
