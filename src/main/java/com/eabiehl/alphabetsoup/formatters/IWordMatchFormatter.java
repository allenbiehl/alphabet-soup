package com.eabiehl.alphabetsoup.formatters;

import com.eabiehl.alphabetsoup.data.WordMatch;

/**
 * Interface used to format the output of a {@see WordMatch} instance. Interfaces
 * are used to ensure that alternate implementations can be substituted when needed.
 * 
 * @author eabiehl
 */
public interface IWordMatchFormatter {

	/**
	 * Returns the formatted output of a {@see WordMatch} instance.
	 * 
	 * @param wordMatch {@see WordMatch} instance to format.
	 * @return Formatted {@see WordMatch} output string.
	 */
	String format(WordMatch wordMatch);
}
