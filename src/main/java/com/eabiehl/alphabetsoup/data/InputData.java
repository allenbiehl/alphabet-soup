package com.eabiehl.alphabetsoup.data;

import java.util.List;

/**
 * Data model for storing the program input, which includes the predefined
 * two dimensional character grid and the list of words contained within
 * the character grid.
 * 
 * @author eabiehl
 */
public class InputData {
	
	/**
	 * Two dimensional grid.
	 */
	private Grid grid;
	
	/**
	 * List of words to match.
	 */
	private List<Word> words;
	
	/**
	 * Constructor for initializing a {@see InputData} instance with a 
	 * populated two dimensional grid and list of words to match.
	 * 
	 * @param grid Two dimensional character grid.
	 * @param words List of words to match.
	 */
	public InputData(Grid grid, List<Word> words) {
		this.grid = grid;
		this.words = words;
	}
	
	/**
	 * Returns the two dimensional character grid.
	 * @return Two dimensional character grid.
	 */
	public Grid getGrid() {
		return this.grid;
	}
	
	/**
	 * Returns the list of words to match against.
	 * @return word list.
	 */
	public List<Word> getWords() {
		return this.words;
	}
}
