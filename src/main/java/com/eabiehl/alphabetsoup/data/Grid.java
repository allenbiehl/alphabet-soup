package com.eabiehl.alphabetsoup.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Data model for storing a two dimensional grid of characters that contains 
 * word matches.
 * 
 * @author eabiehl
 */
public class Grid {
	private char[][] cells = null;
    private Logger logger;
    
    /**
     * Constructor for initializing a {@see Grid} instance with 0 rows and 0 columns.
     */
	public Grid() {
		this(0, 0);
	}
	
	/**
	 * Constructor for initializing a {@see Grid} instance with a fixed size.
	 * 
	 * @param rows Total number of rows to initialize.
	 * @param columns Total number of columns to initialize.
	 */
	public Grid(int rows, int columns) {
		this.cells = new char[rows][columns];
	    this.logger = LoggerFactory.getLogger(Grid.class);
	}
	
	/**
	 * Insert character at the specific row index and column index. If the
	 * row index and column index are within the grid dimensions, then true
	 * is returned, otherwise false is returned.
	 * 
	 * @param rowIdx Index of the row.
	 * @param colIdx Index of the column.
	 * @param value Character to store in this cell location.
	 * @return Whether the character was successfully inserted into the grid.
	 */
	public boolean setCellValue(int rowIdx, int colIdx, char value) {
		if (rowIdx >= this.getTotalRows() || rowIdx < 0) {
			return false;
		}
		
		if (colIdx >= this.getTotalColumns() || colIdx < 0) {
			return false;
		}
		
		this.logger.debug(String.format("Set grid cell value, rowIdx: %s, colIdx: %s, value: %s", 
			rowIdx, colIdx, value));
		
		this.cells[rowIdx][colIdx] = value;
		return true;
	}
	
	/**
	 * Returns character as the specific row index and column index. If the
	 * row index and column index are within the grid dimensions, the the
	 * associated character stored in that cell is returned otherwise null
	 * is returned.
	 * 
	 * @param rowIdx Index of the row.
	 * @param colIdx Index of the column.
	 * @return Character stored in row idx and column idx location.
	 */
	public char getCellValue(int rowIdx, int colIdx) {
		if (rowIdx >= this.getTotalRows() || rowIdx < 0) {
			return 0;
		}
		
		if (colIdx >= this.getTotalColumns() || colIdx < 0) {
			return 0;
		}
		
		return this.cells[rowIdx][colIdx];
	}
	
	/**
	 * Returns the two dimensional grid of character cells. 
	 * 
	 * @return
	 */
	public char[][] getCells() {
		return this.cells;
	}
	
	/**
	 * Returns the total number of rows for this grid.
	 * 
	 * @return Row size dimension.
	 */
	public int getTotalRows() {
		return this.cells.length;
	}
	
	/**
	 * Returns the total number of columns for this grid.
	 * 
	 * @return Column size dimension.
	 */
	public int getTotalColumns() {
		return this.cells.length > 0 
			? this.cells[0].length 
			: 0;
	}
}
