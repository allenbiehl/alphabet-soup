package com.eabiehl.alphabetsoup.data;

/**
 * Data model for storing the location of a character contained within a
 * word match. Once a character is identified in a word match, we record
 * both the row index location, column index location, and the character
 * that was matched against. A sequence of {@See WordMatchCell} instances
 * provides the location of all word match characters found in the grid.
 * 
 * @author eabiehl
 */
public class WordMatchCell {
	
	/**
	 * Match row idx. 
	 */
	private int rowIdx;
	
	/**
	 * Match column idx.
	 */
	private int colIdx;
	
	/**
	 * Match character value.
	 */
	private char value;
	
	/**
	 * Constructor for initializing a {@see WordMatchCell} instance. 
	 * 
	 * @param rowIdx Index of the row associated with the character match.
	 * @param colIdx Index of the column associated with the character match.
	 * @param value Character value stored in cell
	 */
	public WordMatchCell(int rowIdx, int colIdx, char value) {
		this.rowIdx = rowIdx;
		this.colIdx = colIdx;
		this.value = value;
	}

	/**
	 * Returns the row index associated with the location of the word character match.
	 * 
	 * @return Row index associated with the cell.
	 */
	public int getRowIdx() {
		return this.rowIdx;
	}

	/**
	 * Returns the column index associated with the location of the word character match.
	 * 
	 * @return Column index associated with the cell.
	 */
	public int getColumnIdx() {
		return this.colIdx;
	}
	
	/**
	 * Returns the character stored in the cell that was matched.
	 * 
	 * @return Character stores in the cell.
	 */
	public char getValue() {
		return this.value;
	}
}
