package com.eabiehl.alphabetsoup.data;

import java.util.List;

/**
 * Data model for storing a word match containing both the original word
 * that was matched in the character grid as well as the grid cells that
 * contain the coordinates of the word in the grid. 
 * 
 * @author eabiehl
 */
public class WordMatch {
	
	/**
	 * Word that was matched. 
	 */
	private Word word;
	
	/**
	 * Location of all grid cells that were matched.
	 */
	private List<WordMatchCell> cells;
	
	/**
	 * Constructor for initializing a {@see WordMatch} instance that
	 * is associated with a matched word and the list of matched cells
	 * that represent the location of the word.
	 * 
	 * @param word
	 * @param cells
	 */
	public WordMatch(Word word, List<WordMatchCell> cells) {
		this.word = word;
		this.cells = cells;
	}

	/**
	 * Returns original word that was matched against.
	 * @return Original word.
	 */
	public Word getWord() {
		return word;
	}

	/**
	 * Returns the grids cells that provide the location of the word in 
	 * the grid.
	 * @return Grid cells.
	 */
	public List<WordMatchCell> getCells() {
		return cells;
	}
}
