package com.eabiehl.alphabetsoup.data;

/**
 * Data model for storing the word value as well as helper methods
 * for retrieving the compressed word value.
 * 
 * @author eabiehl
 */
public class Word {

	/**
	 * Original word value.
	 */
	private String value;
	
	/**
     * Constructor for initializing a {@see Word} instance using the 
     * original word value.
     * 
	 * @param value Original word value.
	 */
	public Word(String value) {
		this.value = value;
	}
	
	/**
	 * Returns the original word value.
	 * 
	 * @return Original word value.
	 */
	public String getValue() {
		return this.value;
	}
	
	/**
	 * Returns the compressed word value, which strips all spaces contained within 
	 * the word.
	 * 
	 * @return Compressed word value where spaces are removed.
	 */
	public String getCompressedValue() {
		return this.value.replaceAll("\\s", "");
	}
	
	/**
	 * Override to string method which is using for logging.
	 */
	@Override
	public String toString() {
		return this.value;
	}
}
