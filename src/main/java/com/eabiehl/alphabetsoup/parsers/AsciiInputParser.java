package com.eabiehl.alphabetsoup.parsers;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eabiehl.alphabetsoup.data.Grid;
import com.eabiehl.alphabetsoup.data.InputData;
import com.eabiehl.alphabetsoup.data.Word;

/**
 * File stream parser implementation used to parse an ASCII formatted file.
 * 
 * Example: {@see resources/input/example.txt}
 * 
 * @author eabiehl
 */
public class AsciiInputParser extends AbstractInputParser {
	
	/**
     * Logger instance.
	 */
    private Logger logger;
    
    /**
     * Constructor for initializing a {@see AsciiStreamParser} instance.
     */
	public AsciiInputParser() {
		super();
	    this.logger = LoggerFactory.getLogger(AsciiInputParser.class);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected InputData read(InputStream inputStream) {
        Scanner scanner = new Scanner(inputStream);
        
        try {
        	Grid grid = this.parseGrid(scanner);
        	List<Word> words = this.parseWords(scanner);
        	return new InputData(grid, words);
        }
        finally
        {
            scanner.close();
        }
	}
	
	/**
	 * Parse character two dimensional grid from input source.
	 * 
	 * @param scanner {@see Scanner} used to read all lines contained in the input file.
	 * @return Populated two dimensional grid.
	 */
	private Grid parseGrid(Scanner scanner) {
		this.logger.debug("Parsing grid");
		
		String[] dimensions = scanner.nextLine().toLowerCase().split("x");
		
		int totalRows = Integer.parseInt(dimensions[0]);
		int totalCols = Integer.parseInt(dimensions[1]);
		
		this.logger.debug(String.format("Grid dimensions, rows: %s, cols: %s", 
			totalRows, totalCols));
		
		Grid grid = new Grid(totalRows, totalCols);
		
		for (int rowIdx = 0; rowIdx < totalRows; rowIdx++) {
			String line = scanner.nextLine();
			String[] lineValues = line.split("\\s");
			int lineNum = rowIdx + 1;
			int lineCols = lineValues.length;

			this.logger.debug(String.format("Parse grid line, line #: %s, cols: %s, value: %s", 
				lineNum, lineCols, line));
						
			for (int colIdx = 0; colIdx < lineCols; colIdx++) {
				grid.setCellValue(rowIdx, colIdx, lineValues[colIdx].charAt(0));
			}
		}
	
        return grid;
	}
	
	/**
	 * Parse words to match from input source.
	 * 
	 * @param scanner {@see Scanner} used to read all lines contained in the input file.
	 * @return List of words contained in the two dimensional grid.
	 */
	private List<Word> parseWords(Scanner scanner) {
		this.logger.debug("Parsing words");
		List<Word> words = new ArrayList<Word>();
		
        while(scanner.hasNextLine()) {
        	String line = scanner.nextLine().trim(); 
        	Word word = new Word(line);
    		this.logger.debug(String.format("Word match: %s", word));
            words.add(word);
        }
        return words;
	}
}
