package com.eabiehl.alphabetsoup.parsers;

import java.io.InputStream;

import com.eabiehl.alphabetsoup.data.InputData;

/**
 * Interface used to parse an alphabet soup input file. Interfaces are used
 * to ensure that alternate implementations can be substituted when needed.
 * 
 * Example: {@see resources/input/example.txt}
 * 
 * @author eabiehl
 */
public interface IInputParser {

	/**
	 * Parse input based on resource path.
	 * 
	 * @param resourcePath File path where the input file is located. This can
	 * be either a file system path or be located on the class path.
	 * @return Parsed {@see InputData} instance containing the two dimensional 
	 * grid and list of words to match.
	 */
	InputData parse(String resourcePath);
	
	/**
	 * Parse input stream.
	 * 
	 * @param input {@see InputStream} that contains the data stream to parse.
	 * @return Parsed {@see InputData} instance containing the two dimensional 
	 * grid and list of words to match.
	 */
	InputData parse(InputStream input);
}
