package com.eabiehl.alphabetsoup;

import java.util.List;

import com.eabiehl.alphabetsoup.data.WordMatch;
import com.eabiehl.alphabetsoup.formatters.DefaultWordMatchFormatter;
import com.eabiehl.alphabetsoup.formatters.IWordMatchFormatter;
import com.eabiehl.alphabetsoup.processors.DefaultProcessor;
import com.eabiehl.alphabetsoup.processors.IProcessor;

/**
 * Program used to find words contained in a grid of characters.
 * 
 * @author eabiehl
 */
public class Program 
{
	/**
	 * Prevent instantiation of {@see Program}.
	 */
	private Program() {
	}
	
	/**
	 * Program entry point.
	 * 
	 * @param args Command line arguments passed to the program.
	 */
    public static void main( String[] args ) {    	
    	if (args.length > 0) {
	    	IProcessor processor = new DefaultProcessor();
	    	List<WordMatch> matches = processor.process(args[0]);
	    	IWordMatchFormatter formatter = new DefaultWordMatchFormatter();
	    	
	    	for (WordMatch match : matches) {
	    		System.out.println(formatter.format(match));
	    	}
    	}
    }
}
