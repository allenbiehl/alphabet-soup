package com.eabiehl.alphabetsoup.processors;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eabiehl.alphabetsoup.data.InputData;
import com.eabiehl.alphabetsoup.data.WordMatch;
import com.eabiehl.alphabetsoup.matchers.IWordMatcher;
import com.eabiehl.alphabetsoup.matchers.RadialWordMatcher;
import com.eabiehl.alphabetsoup.parsers.AsciiInputParser;
import com.eabiehl.alphabetsoup.parsers.IInputParser;

/**
 * Default implementation used to process an alphabet soup input file. 
 * 
 * @author eabiehl
 */
public class DefaultProcessor implements IProcessor {
	
	/**
	 * Stream parser implementation.
	 */
    private IInputParser parser;
    
    /**
     * Word matcher implementation.
     */
    private IWordMatcher matcher;
    
    /**
     * Logger instance.
     */
    private Logger logger;
    
    /**
     * Constructor for initializing a {@see DefaultProcessor} instance.
     */
    public DefaultProcessor() {
    	this.parser = new AsciiInputParser();
    	this.matcher = new RadialWordMatcher();
        this.logger = LoggerFactory.getLogger(DefaultProcessor.class);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public List<WordMatch> process(String filePath) {
		this.logger.debug("AlphabetSoup process started");
    	InputData inputData = this.parser.parse(filePath);
    	
		// Verify parsed params
		if (inputData != null) {
			return this.matcher.findMatches(inputData.getGrid(), inputData.getWords());
		}

		return new ArrayList<WordMatch>();
    }
}
