package com.eabiehl.alphabetsoup.processors;

import java.util.List;

import com.eabiehl.alphabetsoup.data.WordMatch;

/**
 * Interface used to process an alphabet soup input file. Interfaces are used
 * to ensure that alternate implementations can be substituted when needed.
 * 
 * Example: {@see resources/input/example.txt}
 * 
 * @author eabiehl
 */
public interface IProcessor {
	
	/**
	 * Processes the specified alphabet soup input file and returns
	 * all word matches contained within the two dimensional grid.
	 * @param filePath Input file path.
	 * @return List of word matches.
	 */
    List<WordMatch> process(String filePath);
}
